package io.gitlab.michaelgliwinski.wandisco.techassess;

public interface Shape {
    /**
     * Return the volume of the shape
     *
     * @return double volume
     */
    double getVolume();
}
