package io.gitlab.michaelgliwinski.wandisco.techassess;

import com.google.common.base.Objects;

public class RegularTetrahedron implements Shape {
    private final double edge;

    public RegularTetrahedron(double edge) {
        this.edge = edge;
    }

    private double getEdge() {
        return edge;
    }

    /**
     * Return the volume of the regular tetrahedron
     * <p>
     * The formula is: \(\frac{edge^3}{6 \sqrt{2}}\)
     *
     * @return the volume
     */
    @Override
    public double getVolume() {
        return Math.pow(getEdge(), 3) / (6 * Math.sqrt(2));
    }

    @Override
    public String toString() {
        return "RegularTetrahedron(edge=" + getEdge() + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegularTetrahedron that = (RegularTetrahedron) o;
        return Double.compare(that.getEdge(), getEdge()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getEdge());
    }
}
