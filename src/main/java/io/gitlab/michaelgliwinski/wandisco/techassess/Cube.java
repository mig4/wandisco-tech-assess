package io.gitlab.michaelgliwinski.wandisco.techassess;

import com.google.common.base.Objects;

public class Cube implements Shape {
    private final double width;

    public Cube(double width) {
        this.width = width;
    }

    private double getWidth() {
        return width;
    }

    /**
     * Return the volume of the cube
     * <p>
     * The formula is: \(width^3\)
     *
     * @return the volume
     */
    @Override
    public double getVolume() {
        return Math.pow(getWidth(), 3);
    }

    @Override
    public String toString() {
        return "Cube(width=" + getWidth() + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cube cube = (Cube) o;
        return Double.compare(cube.getWidth(), getWidth()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getWidth());
    }
}
