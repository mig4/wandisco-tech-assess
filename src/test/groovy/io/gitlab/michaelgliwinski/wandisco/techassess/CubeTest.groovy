package io.gitlab.michaelgliwinski.wandisco.techassess

import spock.lang.Specification
import spock.lang.Unroll

class CubeTest extends Specification {
    @Unroll
    def "it asserts a cube of width #width has volume #volume"(long width, long volume) {
        given:
        def cube = new Cube(width)

        expect:
        cube.volume == volume

        where:
        width | volume
        1     | 1
        5     | 125
    }
}
