package io.gitlab.michaelgliwinski.wandisco.techassess

import spock.lang.Specification

abstract class ShapeTestBase extends Specification {
    /**
     * Round number to two decimal places
     *
     * @param num the number to round
     * @return the rounded number
     */
    protected double roundTo2(double num) {
        Math.round(num * 100) / 100
    }
}
