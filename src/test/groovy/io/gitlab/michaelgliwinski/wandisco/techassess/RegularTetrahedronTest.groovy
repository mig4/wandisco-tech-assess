package io.gitlab.michaelgliwinski.wandisco.techassess

import spock.lang.Unroll

class RegularTetrahedronTest extends ShapeTestBase {
    @Unroll
    def "it asserts a regular tetrahedron with edge #edge has volume #volume"(double edge, double volume) {
        given:
        def tetrahedron = new RegularTetrahedron(edge)

        expect:
        roundTo2(tetrahedron.volume) == volume

        where:
        edge | volume
        1    | 0.12
        5    | 14.73
    }
}
