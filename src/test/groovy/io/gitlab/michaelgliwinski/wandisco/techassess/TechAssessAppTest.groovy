package io.gitlab.michaelgliwinski.wandisco.techassess

import spock.lang.Specification

import java.util.stream.Collectors

class TechAssessAppTest extends Specification {
    def "application has all required shapes"() {
        given:
        def app = new TechAssessApp()
        def expected = [
                new Sphere(1),
                new Sphere(5),
                new Cube(1),
                new Cube(5),
                new RegularTetrahedron(1),
                new RegularTetrahedron(5),
        ]

        expect:
        app.shapes.collect(Collectors.toList()) == expected
    }
}
