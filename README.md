# WANdisco tech assessment

CI runs unit tests which assert the expected results, the test report
is published to:

https://michael.gliwinski.gitlab.io/wandisco-tech-assess/test/

(you may get an SSL certificate error as GitLab cert doesn't apply to
subdomains and I have a '.' in my username), alternatively via HTTP:

http://michael.gliwinski.gitlab.io/wandisco-tech-assess/test/

Alternatively to run locally, either clone the repo or download a ZIP
from:

https://gitlab.com/michael.gliwinski/wandisco-tech-assess/repository/master/archive.zip

extract and run:

``` sh
./gradlew check
```

to run the tests, and:

``` sh
./gradlew run
```

to run the app and display results to the console.
